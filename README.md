
# Genesis

Inputs to genesis, in order of precedence

1. [primary.csv](./primary.csv) are the primary ADIs that will be created at
  genesis, and will own all the other ADIs created at Genesis.
2. [operators.csv](./operators.csv) are the initial Operators for Accumulate at
  Genesis.
3. [internal-names-list.csv](./internal-names-list.csv) are ADIs reserved by the
  core team, support staff, partners, and initial operators. Initially
  controlled by defidevs.acme, governance.acme, or another primary ADI. Control
  to be transferred on request.
4. [other-blockchains-list.csv](./other-blockchains-list.csv) are ADIs reserved
  for cross-chain initiatives. Controlled by accumulate.acme.
5. [fortune-500-list.csv](./fortune-500-list.csv) are ADIs reserved for Fortune
  500 companies. Initially controlled by accumulate.acme, control to be
  transferred on request (after verification).
6. [popular-words-list.csv](./popular-words-list.csv) are ADIs reserved for the
  marketplace, made up of popular words in the english language. Controlled by marketplace.acme.
7. [top-10000-list-1-3-chars.csv](./top-10000-list-1-3-chars.csv) are ADIs reserved for the
  marketplace, made up of the top 10,000 domain names by Alexa rank between 1-3 characters. Controlled by marketplace.acme.
8. [top-10000-list-4-chars.csv](./top-10000-list-4-chars.csv) are ADIs reserved for the
  marketplace, made up of the top 10,000 domain names by Alexa rank with 4 characters. Controlled by marketplace.acme.
9. [top-10000-list-5-chars.csv](./top-10000-list-5-chars.csv) are ADIs reserved for the
  marketplace, made up of the top 10,000 domain names by Alexa rank with 5 characters. Controlled by marketplace.acme.

